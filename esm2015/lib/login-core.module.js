var LoginCoreModule_1;
import { __decorate } from "tslib";
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LOGIN_REPOSITORY } from './repositories/ILogin.repository';
import { LoginRepository } from './repositories/login.repository';
import { LOGIN_SERVICE } from './services/ILogin.service';
import { LoginService } from './services/login.service';
import { LoginEffects } from './state/login.effects';
import { loginReducer } from './state/login.reducer';
import { LoginStore } from './state/login.store';
let LoginCoreModule = LoginCoreModule_1 = class LoginCoreModule {
    static forRoot(config) {
        return {
            ngModule: LoginCoreModule_1,
            providers: [
                { provide: LOGIN_SERVICE, useClass: LoginService },
                { provide: LOGIN_REPOSITORY, useClass: LoginRepository },
                ...config.providers,
                LoginStore
            ]
        };
    }
};
LoginCoreModule = LoginCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('login', loginReducer),
            EffectsModule.forFeature([LoginEffects]),
        ],
        exports: []
    })
], LoginCoreModule);
export { LoginCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9sb2dpbi1jb3JlLyIsInNvdXJjZXMiOlsibGliL2xvZ2luLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QixRQUFRLEVBQVksTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNsRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBZWpELElBQWEsZUFBZSx1QkFBNUIsTUFBYSxlQUFlO0lBQ3hCLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBOEI7UUFFekMsT0FBTztZQUNILFFBQVEsRUFBRSxpQkFBZTtZQUN6QixTQUFTLEVBQUU7Z0JBQ1AsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUU7Z0JBQ2xELEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUU7Z0JBQ3hELEdBQUcsTUFBTSxDQUFDLFNBQVM7Z0JBQ25CLFVBQVU7YUFDYjtTQUNKLENBQUM7SUFDTixDQUFDO0NBQ0gsQ0FBQTtBQWJXLGVBQWU7SUFUM0IsUUFBUSxDQUFDO1FBQ04sWUFBWSxFQUFFLEVBQUU7UUFDaEIsT0FBTyxFQUFFO1lBQ0wsZ0JBQWdCO1lBQ2hCLFdBQVcsQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQztZQUM3QyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDM0M7UUFDRCxPQUFPLEVBQUUsRUFBRTtLQUNkLENBQUM7R0FDVyxlQUFlLENBYTFCO1NBYlcsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEVmZmVjdHNNb2R1bGUgfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcbmltcG9ydCB7IFN0b3JlTW9kdWxlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgTE9HSU5fUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lMb2dpbi5yZXBvc2l0b3J5JztcbmltcG9ydCB7IExvZ2luUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL2xvZ2luLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgTE9HSU5fU0VSVklDRSB9IGZyb20gJy4vc2VydmljZXMvSUxvZ2luLnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9naW5TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9sb2dpbi5zZXJ2aWNlJztcbmltcG9ydCB7IExvZ2luRWZmZWN0cyB9IGZyb20gJy4vc3RhdGUvbG9naW4uZWZmZWN0cyc7XG5pbXBvcnQgeyBsb2dpblJlZHVjZXIgfSBmcm9tICcuL3N0YXRlL2xvZ2luLnJlZHVjZXInO1xuaW1wb3J0IHsgTG9naW5TdG9yZSB9IGZyb20gJy4vc3RhdGUvbG9naW4uc3RvcmUnO1xuXG5pbnRlcmZhY2UgTW9kdWxlT3B0aW9uc0ludGVyZmFjZSB7XG4gICAgcHJvdmlkZXJzOiBQcm92aWRlcltdO1xufVxuXG5ATmdNb2R1bGUoe1xuICAgIGRlY2xhcmF0aW9uczogW10sXG4gICAgaW1wb3J0czogW1xuICAgICAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgICAgICBTdG9yZU1vZHVsZS5mb3JGZWF0dXJlKCdsb2dpbicsIGxvZ2luUmVkdWNlciksXG4gICAgICAgIEVmZmVjdHNNb2R1bGUuZm9yRmVhdHVyZShbTG9naW5FZmZlY3RzXSksXG4gICAgXSxcbiAgICBleHBvcnRzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkNvcmVNb2R1bGUge1xuICAgIHN0YXRpYyBmb3JSb290KGNvbmZpZzogTW9kdWxlT3B0aW9uc0ludGVyZmFjZSk6IE1vZHVsZVdpdGhQcm92aWRlcnM8TG9naW5Db3JlTW9kdWxlPiB7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG5nTW9kdWxlOiBMb2dpbkNvcmVNb2R1bGUsXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IExPR0lOX1NFUlZJQ0UsIHVzZUNsYXNzOiBMb2dpblNlcnZpY2UgfSxcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IExPR0lOX1JFUE9TSVRPUlksIHVzZUNsYXNzOiBMb2dpblJlcG9zaXRvcnkgfSxcbiAgICAgICAgICAgICAgICAuLi5jb25maWcucHJvdmlkZXJzLFxuICAgICAgICAgICAgICAgIExvZ2luU3RvcmVcbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICB9XG4gfVxuIl19