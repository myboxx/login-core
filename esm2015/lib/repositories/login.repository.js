import { __decorate, __param } from "tslib";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { IHttpBasicResponse, APP_CONFIG_SERVICE, AbstractAppConfigService } from '@boxx/core';
let LoginRepository = class LoginRepository {
    constructor(http, appSettings) {
        this.http = http;
        this.appSettings = appSettings;
        this.REFRESH_TOKEN_URL = '/api/v1/auth/refresh_token';
    }
    login(credentials) {
        let params = new HttpParams();
        for (const property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }
        const body = params.toString();
        return this.http.post(`${this.appSettings.baseUrl()}/api/v2/auth/login`, body);
    }
    socialLogin(credentials) {
        let params = new HttpParams();
        for (const property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }
        const body = params.toString();
        return this.http.post(`${this.appSettings.baseUrl()}/api/v2/auth/add_social`, body);
    }
    saveToken(deviceId, fcmRegistrationId) {
        const data = { device_id: deviceId, fcm_registration_id: fcmRegistrationId };
        let params = new HttpParams();
        for (const property in data) {
            if (data.hasOwnProperty(property)) {
                params = params.append(property, data[property]);
            }
        }
        const body = params.toString();
        return this.http.post(`${this.getBaseUrl()}/fcm/devices`, body);
    }
    logSessionStart(payload) {
        const params = new HttpParams()
            .append('action_type', payload.action_type);
        // .append('action_desciption', payload.action_description || 'NO_DESCRIPTION_SUPPLIED');
        const urlSearchParams = new URLSearchParams();
        urlSearchParams.append('action_type', payload.action_type);
        const body = params.toString();
        return this.http.post(`${this.appSettings.baseUrl()}/api/v1/profileUser/check_app_login`, body);
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1`;
    }
};
LoginRepository.ctorParameters = () => [
    { type: HttpClient },
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] }
];
LoginRepository = __decorate([
    Injectable(),
    __param(1, Inject(APP_CONFIG_SERVICE))
], LoginRepository);
export { LoginRepository };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4ucmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L2xvZ2luLWNvcmUvIiwic291cmNlcyI6WyJsaWIvcmVwb3NpdG9yaWVzL2xvZ2luLnJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFVbkQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixFQUFFLHdCQUF3QixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBRzlGLElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUFHeEIsWUFDWSxJQUFnQixFQUNZLFdBQXFDO1FBRGpFLFNBQUksR0FBSixJQUFJLENBQVk7UUFDWSxnQkFBVyxHQUFYLFdBQVcsQ0FBMEI7UUFKcEUsc0JBQWlCLEdBQVcsNEJBQTRCLENBQUM7SUFLOUQsQ0FBQztJQUVMLEtBQUssQ0FBQyxXQUEwQztRQUU1QyxJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBRTlCLEtBQUssTUFBTSxRQUFRLElBQUksV0FBVyxFQUFFO1lBQ2hDLElBQUksV0FBVyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDdEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2FBQzNEO1NBQ0o7UUFFRCxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFL0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBeUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzSCxDQUFDO0lBRUQsV0FBVyxDQUFDLFdBQTZCO1FBRXJDLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFFOUIsS0FBSyxNQUFNLFFBQVEsSUFBSSxXQUFXLEVBQUU7WUFDaEMsSUFBSSxXQUFXLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN0QyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7YUFDM0Q7U0FDSjtRQUVELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUUvQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUErQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLHlCQUF5QixFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3RJLENBQUM7SUFFRCxTQUFTLENBQUMsUUFBZ0IsRUFBRSxpQkFBeUI7UUFDakQsTUFBTSxJQUFJLEdBQUcsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLG1CQUFtQixFQUFFLGlCQUFpQixFQUFFLENBQUM7UUFFN0UsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztRQUU5QixLQUFLLE1BQU0sUUFBUSxJQUFJLElBQUksRUFBRTtZQUN6QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQy9CLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzthQUNwRDtTQUNKO1FBRUQsTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRS9CLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsZUFBZSxDQUFDLE9BQTJCO1FBRXZDLE1BQU0sTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFO2FBQzFCLE1BQU0sQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2hELHlGQUF5RjtRQUd6RixNQUFNLGVBQWUsR0FBRyxJQUFJLGVBQWUsRUFBRSxDQUFDO1FBQzlDLGVBQWUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUUzRCxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFL0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDakIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxxQ0FBcUMsRUFBRSxJQUFJLENBQzNFLENBQUM7SUFDTixDQUFDO0lBRU8sVUFBVTtRQUNkLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxRQUFRLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQztJQUNqRixDQUFDO0NBQ0osQ0FBQTs7WUF0RXFCLFVBQVU7WUFDeUIsd0JBQXdCLHVCQUF4RSxNQUFNLFNBQUMsa0JBQWtCOztBQUxyQixlQUFlO0lBRDNCLFVBQVUsRUFBRTtJQU1KLFdBQUEsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7R0FMdEIsZUFBZSxDQTBFM0I7U0ExRVksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gICAgSUxvZ2luRGF0YVJlc3BvbnNlLFxuICAgIElMb2dpbkZvcm0sXG4gICAgSUxvZ2luUmVwb3NpdG9yeSxcbiAgICBJTG9nU2Vzc2lvblBheWxvYWQsXG4gICAgSVNhdmVUb2tlbkRhdGFSZXNwb25zZSxcbiAgICBJU29jaWFsTG9naW5EYXRhUmVzcG9uc2UsXG4gICAgSVNvY2lhbExvZ2luRm9ybVxufSBmcm9tICcuL0lMb2dpbi5yZXBvc2l0b3J5JztcbmltcG9ydCB7IElIdHRwQmFzaWNSZXNwb25zZSwgQVBQX0NPTkZJR19TRVJWSUNFLCBBYnN0cmFjdEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICdAYm94eC9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvZ2luUmVwb3NpdG9yeSBpbXBsZW1lbnRzIElMb2dpblJlcG9zaXRvcnkge1xuICAgIHJlYWRvbmx5IFJFRlJFU0hfVE9LRU5fVVJMOiBzdHJpbmcgPSAnL2FwaS92MS9hdXRoL3JlZnJlc2hfdG9rZW4nO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcbiAgICAgICAgQEluamVjdChBUFBfQ09ORklHX1NFUlZJQ0UpIHByaXZhdGUgYXBwU2V0dGluZ3M6IEFic3RyYWN0QXBwQ29uZmlnU2VydmljZVxuICAgICkgeyB9XG5cbiAgICBsb2dpbihjcmVkZW50aWFsczogSUxvZ2luRm9ybSB8IElTb2NpYWxMb2dpbkZvcm0pOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJTG9naW5EYXRhUmVzcG9uc2U+PiB7XG5cbiAgICAgICAgbGV0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCk7XG5cbiAgICAgICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiBjcmVkZW50aWFscykge1xuICAgICAgICAgICAgaWYgKGNyZWRlbnRpYWxzLmhhc093blByb3BlcnR5KHByb3BlcnR5KSkge1xuICAgICAgICAgICAgICAgIHBhcmFtcyA9IHBhcmFtcy5hcHBlbmQocHJvcGVydHksIGNyZWRlbnRpYWxzW3Byb3BlcnR5XSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBib2R5ID0gcGFyYW1zLnRvU3RyaW5nKCk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0PElIdHRwQmFzaWNSZXNwb25zZTxJTG9naW5EYXRhUmVzcG9uc2U+PihgJHt0aGlzLmFwcFNldHRpbmdzLmJhc2VVcmwoKX0vYXBpL3YyL2F1dGgvbG9naW5gLCBib2R5KTtcbiAgICB9XG5cbiAgICBzb2NpYWxMb2dpbihjcmVkZW50aWFsczogSVNvY2lhbExvZ2luRm9ybSk6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElTb2NpYWxMb2dpbkRhdGFSZXNwb25zZT4+IHtcblxuICAgICAgICBsZXQgcGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKTtcblxuICAgICAgICBmb3IgKGNvbnN0IHByb3BlcnR5IGluIGNyZWRlbnRpYWxzKSB7XG4gICAgICAgICAgICBpZiAoY3JlZGVudGlhbHMuaGFzT3duUHJvcGVydHkocHJvcGVydHkpKSB7XG4gICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChwcm9wZXJ0eSwgY3JlZGVudGlhbHNbcHJvcGVydHldKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGJvZHkgPSBwYXJhbXMudG9TdHJpbmcoKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8SUh0dHBCYXNpY1Jlc3BvbnNlPElTb2NpYWxMb2dpbkRhdGFSZXNwb25zZT4+KGAke3RoaXMuYXBwU2V0dGluZ3MuYmFzZVVybCgpfS9hcGkvdjIvYXV0aC9hZGRfc29jaWFsYCwgYm9keSk7XG4gICAgfVxuXG4gICAgc2F2ZVRva2VuKGRldmljZUlkOiBzdHJpbmcsIGZjbVJlZ2lzdHJhdGlvbklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJU2F2ZVRva2VuRGF0YVJlc3BvbnNlPj4ge1xuICAgICAgICBjb25zdCBkYXRhID0geyBkZXZpY2VfaWQ6IGRldmljZUlkLCBmY21fcmVnaXN0cmF0aW9uX2lkOiBmY21SZWdpc3RyYXRpb25JZCB9O1xuXG4gICAgICAgIGxldCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpO1xuXG4gICAgICAgIGZvciAoY29uc3QgcHJvcGVydHkgaW4gZGF0YSkge1xuICAgICAgICAgICAgaWYgKGRhdGEuaGFzT3duUHJvcGVydHkocHJvcGVydHkpKSB7XG4gICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChwcm9wZXJ0eSwgZGF0YVtwcm9wZXJ0eV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgYm9keSA9IHBhcmFtcy50b1N0cmluZygpO1xuXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxhbnk+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9mY20vZGV2aWNlc2AsIGJvZHkpO1xuICAgIH1cblxuICAgIGxvZ1Nlc3Npb25TdGFydChwYXlsb2FkOiBJTG9nU2Vzc2lvblBheWxvYWQpIHtcblxuICAgICAgICBjb25zdCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpXG4gICAgICAgICAgICAuYXBwZW5kKCdhY3Rpb25fdHlwZScsIHBheWxvYWQuYWN0aW9uX3R5cGUpO1xuICAgICAgICAvLyAuYXBwZW5kKCdhY3Rpb25fZGVzY2lwdGlvbicsIHBheWxvYWQuYWN0aW9uX2Rlc2NyaXB0aW9uIHx8ICdOT19ERVNDUklQVElPTl9TVVBQTElFRCcpO1xuXG5cbiAgICAgICAgY29uc3QgdXJsU2VhcmNoUGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xuICAgICAgICB1cmxTZWFyY2hQYXJhbXMuYXBwZW5kKCdhY3Rpb25fdHlwZScsIHBheWxvYWQuYWN0aW9uX3R5cGUpO1xuXG4gICAgICAgIGNvbnN0IGJvZHkgPSBwYXJhbXMudG9TdHJpbmcoKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8SUh0dHBCYXNpY1Jlc3BvbnNlPGFueT4+KFxuICAgICAgICAgICAgYCR7dGhpcy5hcHBTZXR0aW5ncy5iYXNlVXJsKCl9L2FwaS92MS9wcm9maWxlVXNlci9jaGVja19hcHBfbG9naW5gLCBib2R5XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRCYXNlVXJsKCkge1xuICAgICAgICByZXR1cm4gYCR7dGhpcy5hcHBTZXR0aW5ncy5iYXNlVXJsKCl9L2FwaS8ke3RoaXMuYXBwU2V0dGluZ3MuaW5zdGFuY2UoKX0vdjFgO1xuICAgIH1cbn1cbiJdfQ==