(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common/http'), require('@angular/core'), require('@ngrx/effects'), require('@ngrx/store'), require('@boxx/core'), require('@auth0/angular-jwt'), require('rxjs'), require('rxjs/operators')) :
    typeof define === 'function' && define.amd ? define('@boxx/login-core', ['exports', '@angular/common/http', '@angular/core', '@ngrx/effects', '@ngrx/store', '@boxx/core', '@auth0/angular-jwt', 'rxjs', 'rxjs/operators'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['login-core'] = {}), global.ng.common.http, global.ng.core, global['ngrx-effects'], global['ngrx-store'], global['boxx-core'], global['auth0-angular-jwt'], global.rxjs, global.rxjs.operators));
}(this, (function (exports, http, core, effects, store, core$1, angularJwt, rxjs, operators) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var LOGIN_REPOSITORY = new core.InjectionToken('loginRepository');

    var LoginRepository = /** @class */ (function () {
        function LoginRepository(http, appSettings) {
            this.http = http;
            this.appSettings = appSettings;
            this.REFRESH_TOKEN_URL = '/api/v1/auth/refresh_token';
        }
        LoginRepository.prototype.login = function (credentials) {
            var params = new http.HttpParams();
            for (var property in credentials) {
                if (credentials.hasOwnProperty(property)) {
                    params = params.append(property, credentials[property]);
                }
            }
            var body = params.toString();
            return this.http.post(this.appSettings.baseUrl() + "/api/v2/auth/login", body);
        };
        LoginRepository.prototype.socialLogin = function (credentials) {
            var params = new http.HttpParams();
            for (var property in credentials) {
                if (credentials.hasOwnProperty(property)) {
                    params = params.append(property, credentials[property]);
                }
            }
            var body = params.toString();
            return this.http.post(this.appSettings.baseUrl() + "/api/v2/auth/add_social", body);
        };
        LoginRepository.prototype.saveToken = function (deviceId, fcmRegistrationId) {
            var data = { device_id: deviceId, fcm_registration_id: fcmRegistrationId };
            var params = new http.HttpParams();
            for (var property in data) {
                if (data.hasOwnProperty(property)) {
                    params = params.append(property, data[property]);
                }
            }
            var body = params.toString();
            return this.http.post(this.getBaseUrl() + "/fcm/devices", body);
        };
        LoginRepository.prototype.logSessionStart = function (payload) {
            var params = new http.HttpParams()
                .append('action_type', payload.action_type);
            // .append('action_desciption', payload.action_description || 'NO_DESCRIPTION_SUPPLIED');
            var urlSearchParams = new URLSearchParams();
            urlSearchParams.append('action_type', payload.action_type);
            var body = params.toString();
            return this.http.post(this.appSettings.baseUrl() + "/api/v1/profileUser/check_app_login", body);
        };
        LoginRepository.prototype.getBaseUrl = function () {
            return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1";
        };
        LoginRepository.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: core$1.AbstractAppConfigService, decorators: [{ type: core.Inject, args: [core$1.APP_CONFIG_SERVICE,] }] }
        ]; };
        LoginRepository = __decorate([
            core.Injectable(),
            __param(1, core.Inject(core$1.APP_CONFIG_SERVICE))
        ], LoginRepository);
        return LoginRepository;
    }());

    var LOGIN_SERVICE = new core.InjectionToken('loginService');
    var NATIVE_STORAGE_SERVICE = new core.InjectionToken('nativeStorageService');

    var UserModel = /** @class */ (function () {
        function UserModel(data) {
            this.id = data.id;
            this.clientId = data.clientId;
            this.name = data.name;
            this.email = data.email;
            this.roleId = data.roleId;
            this.authedAt = data.authedAt;
            this.deviceId = data.deviceId;
        }
        UserModel.fromTokenData = function (data) {
            return new UserModel({
                id: data.id,
                clientId: data.client_id,
                name: data.name,
                email: data.email,
                roleId: data.role_id,
                authedAt: data.authed_at,
                deviceId: data.device_id,
            });
        };
        UserModel.empty = function () {
            return new UserModel({
                id: null,
                clientId: null,
                name: null,
                email: null,
                roleId: null,
                authedAt: null,
                deviceId: null,
            });
        };
        return UserModel;
    }());

    var helper = new angularJwt.JwtHelperService();
    var TOKEN_KEY = 'AUTH_TOKEN';
    function jwtOptionsFactory(localStge) {
        return {
            tokenGetter: function () { return localStge.getItem(TOKEN_KEY); }
        };
    }
    var LoginService = /** @class */ (function () {
        function LoginService(localStge, repository) {
            this.localStge = localStge;
            this.repository = repository;
        }
        LoginService.prototype.login = function (credentials) {
            var _this = this;
            return this.repository.login(credentials).pipe(operators.map(function (response) {
                if (response.status !== 'success' || !response.data.token) {
                    throw new Error('No token was received');
                }
                _this.localStge.setItem(TOKEN_KEY, response.data.token);
                var user = helper.decodeToken(response.data.token);
                var User = UserModel.fromTokenData(user);
                _this.localStge.setItem('username', User.name);
                return User;
            }), operators.catchError(function (e) {
                console.error('CATCH: AuthenticationService.login() -> repository.login()', e);
                throw (e);
            }));
        };
        LoginService.prototype.socialLogin = function (credentials) {
            var _this = this;
            return this.repository.socialLogin(credentials).pipe(operators.map(function (response) {
                if (response.status !== 'success' || !response.data.token) {
                    throw new Error('No token was received');
                }
                _this.localStge.setItem(TOKEN_KEY, response.data.token);
                var user = helper.decodeToken(response.data.token);
                var User = UserModel.fromTokenData(user);
                _this.localStge.setItem('username', User.name);
                return User;
            }), operators.catchError(function (e) {
                console.error('CATCH: AuthenticationService.socialLogin() -> repository.socialLogin()', e);
                throw (e);
            }));
        };
        LoginService.prototype.logout = function () {
            var _this = this;
            return new rxjs.Observable(function (subcriber) {
                _this.localStge.clearStorage()
                    .then(function (_) {
                    _this.localStge.setItem('tutorial', 'OK');
                    subcriber.next(true);
                })
                    .catch(function (e) {
                    console.error('AuthenticationService.logout()->this.localStge.clearStorage()', e);
                    subcriber.error(e);
                });
            });
        };
        LoginService.prototype.saveToken = function (deviceId, deviceToken) {
            return this.repository.saveToken(deviceId, deviceToken).pipe(operators.map(function (response) {
                return response.data;
            }), operators.catchError(function (e) {
                console.error('CATCH: AuthenticationService.saveToken() -> repository.saveToken()', e);
                throw (e);
            }));
        };
        LoginService.prototype.logSessionStart = function () {
            return this.repository.logSessionStart({ action_type: 'login' }).pipe(operators.map(function (response) {
                return response;
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        LoginService.prototype.getSession = function () {
            return this.localStge.getItem(TOKEN_KEY).then(function (token) {
                var user = helper.decodeToken(token);
                var User = UserModel.fromTokenData(user);
                return User;
            });
        };
        LoginService.prototype.getUserName = function () {
            return this.localStge.getItem('username');
        };
        LoginService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [NATIVE_STORAGE_SERVICE,] }] },
            { type: undefined, decorators: [{ type: core.Inject, args: [LOGIN_REPOSITORY,] }] }
        ]; };
        LoginService.ɵprov = core.ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(core.ɵɵinject(NATIVE_STORAGE_SERVICE), core.ɵɵinject(LOGIN_REPOSITORY)); }, token: LoginService, providedIn: "root" });
        LoginService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(0, core.Inject(NATIVE_STORAGE_SERVICE)),
            __param(1, core.Inject(LOGIN_REPOSITORY))
        ], LoginService);
        return LoginService;
    }());


    (function (LoginActionTypes) {
        LoginActionTypes["LoginBegin"] = "[Login] Login begin";
        LoginActionTypes["LoginSuccess"] = "[Login] Login success";
        LoginActionTypes["LoginFail"] = "[Login] Login failure";
        LoginActionTypes["SocialLoginBegin"] = "[Login] Social Login begin";
        LoginActionTypes["SocialLoginSuccess"] = "[Login] Social Login success";
        LoginActionTypes["SocialLoginFail"] = "[Login] Social Login failure";
        LoginActionTypes["LogoutBegin"] = "[Login] Logout begin";
        LoginActionTypes["LogoutSuccess"] = "[Login] Logout success";
        LoginActionTypes["LogoutFail"] = "[Login] Logout failure";
        LoginActionTypes["SetUserData"] = "[Login] Set User Data";
    })(exports.LoginActionTypes || (exports.LoginActionTypes = {}));
    // LOGIN...
    var LoginBeginAction = store.createAction(exports.LoginActionTypes.LoginBegin, store.props());
    var SocialLoginBeginAction = store.createAction(exports.LoginActionTypes.SocialLoginBegin, store.props());
    var LoginSuccessAction = store.createAction(exports.LoginActionTypes.LoginSuccess, store.props());
    var SocialLoginSuccessAction = store.createAction(exports.LoginActionTypes.SocialLoginSuccess, store.props());
    var LoginFailAction = store.createAction(exports.LoginActionTypes.LoginFail, store.props());
    var SocialLoginFailAction = store.createAction(exports.LoginActionTypes.SocialLoginFail, store.props());
    // LOGOUT...
    var LogoutBeginAction = store.createAction(exports.LoginActionTypes.LogoutBegin);
    var LogoutSuccessAction = store.createAction(exports.LoginActionTypes.LogoutSuccess);
    var LogoutFailAction = store.createAction(exports.LoginActionTypes.LogoutFail, store.props());
    var SetUserData = store.createAction(exports.LoginActionTypes.SetUserData, store.props());

    var LoginEffects = /** @class */ (function () {
        function LoginEffects(actions$, service) {
            var _this = this;
            this.actions$ = actions$;
            this.service = service;
            this.login$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.LoginActionTypes.LoginBegin), operators.switchMap(function (action) {
                return _this.service.login(action.credentials).pipe(operators.map(function (user) { return LoginSuccessAction({ user: user }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t login', error);
                    return rxjs.of(LoginFailAction({ errors: error }));
                }));
            })); });
            this.socialLogin$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.LoginActionTypes.SocialLoginBegin), operators.switchMap(function (action) {
                return _this.service.socialLogin(action.credentials).pipe(operators.map(function (user) { return SocialLoginSuccessAction({ user: user }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t add Social login', error);
                    return rxjs.of(SocialLoginFailAction({ errors: error }));
                }));
            })); });
            this.logout$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.LoginActionTypes.LogoutBegin), operators.switchMap(function () {
                return _this.service.logout().pipe(operators.map(function (success) { return LogoutSuccessAction(); }), operators.catchError(function (error) {
                    console.error('Couldn\'t logout', error);
                    return rxjs.of(LogoutFailAction({ errors: error }));
                }));
            })); });
        }
        LoginEffects.ctorParameters = function () { return [
            { type: effects.Actions },
            { type: undefined, decorators: [{ type: core.Inject, args: [LOGIN_SERVICE,] }] }
        ]; };
        LoginEffects = __decorate([
            core.Injectable(),
            __param(1, core.Inject(LOGIN_SERVICE))
        ], LoginEffects);
        return LoginEffects;
    }());

    var initialState = {
        isLoading: false,
        user: UserModel.empty(),
        errors: null
    };
    var ɵ0 = function (state) { return (__assign(__assign({}, state), { errors: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, user: action.user })); }, ɵ2 = function (state) { return (initialState); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { user: action.user })); }, ɵ4 = function (state, action) { return ({
        isLoading: false,
        user: UserModel.empty(),
        errors: action.errors
    }); };
    var reducer = store.createReducer(initialState, 
    // On Begin Actions
    store.on(LoginBeginAction, SocialLoginBeginAction, LogoutBeginAction, ɵ0), 
    // ON Success Actions
    store.on(LoginSuccessAction, SocialLoginSuccessAction, ɵ1), store.on(LogoutSuccessAction, ɵ2), store.on(SetUserData, ɵ3), 
    // ON Fail Actions
    store.on(LoginFailAction, SocialLoginFailAction, LogoutFailAction, ɵ4));
    function loginReducer(state, action) {
        return reducer(state, action);
    }

    var getLoginState = store.createFeatureSelector('login');
    var ɵ0$1 = function (state) { return state; };
    var getLoginPageState = store.createSelector(getLoginState, ɵ0$1);
    var storeGetIsLoading = function (state) { return state.isLoading; };
    var storeGetUser = function (state) { return state.user; };
    var getIsLoading = store.createSelector(getLoginPageState, storeGetIsLoading);
    var getUser = store.createSelector(getLoginPageState, storeGetUser);
    var ɵ1$1 = function (state) { return state.errors; };
    var getError = store.createSelector(getLoginPageState, ɵ1$1);

    var LoginStore = /** @class */ (function () {
        function LoginStore(store) {
            this.store = store;
        }
        Object.defineProperty(LoginStore.prototype, "User$", {
            get: function () {
                return this.store.select(getUser);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginStore.prototype, "Loading$", {
            get: function () {
                return this.store.select(getIsLoading);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginStore.prototype, "Error$", {
            get: function () {
                return this.store.select(getError);
            },
            enumerable: true,
            configurable: true
        });
        LoginStore.prototype.login = function (credentials) {
            this.store.dispatch(LoginBeginAction({ credentials: credentials }));
        };
        LoginStore.prototype.socialLogin = function (credentials) {
            this.store.dispatch(SocialLoginBeginAction({ credentials: credentials }));
        };
        LoginStore.prototype.logout = function () {
            this.store.dispatch(LogoutBeginAction());
        };
        LoginStore.prototype.setUserData = function (user) {
            this.store.dispatch(SetUserData({ user: user }));
        };
        LoginStore.ctorParameters = function () { return [
            { type: store.Store }
        ]; };
        LoginStore = __decorate([
            core.Injectable()
        ], LoginStore);
        return LoginStore;
    }());

    var LoginCoreModule = /** @class */ (function () {
        function LoginCoreModule() {
        }
        LoginCoreModule_1 = LoginCoreModule;
        LoginCoreModule.forRoot = function (config) {
            return {
                ngModule: LoginCoreModule_1,
                providers: __spread([
                    { provide: LOGIN_SERVICE, useClass: LoginService },
                    { provide: LOGIN_REPOSITORY, useClass: LoginRepository }
                ], config.providers, [
                    LoginStore
                ])
            };
        };
        var LoginCoreModule_1;
        LoginCoreModule = LoginCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [],
                imports: [
                    http.HttpClientModule,
                    store.StoreModule.forFeature('login', loginReducer),
                    effects.EffectsModule.forFeature([LoginEffects]),
                ],
                exports: []
            })
        ], LoginCoreModule);
        return LoginCoreModule;
    }());

    exports.LOGIN_REPOSITORY = LOGIN_REPOSITORY;
    exports.LOGIN_SERVICE = LOGIN_SERVICE;
    exports.LoginBeginAction = LoginBeginAction;
    exports.LoginCoreModule = LoginCoreModule;
    exports.LoginEffects = LoginEffects;
    exports.LoginFailAction = LoginFailAction;
    exports.LoginRepository = LoginRepository;
    exports.LoginService = LoginService;
    exports.LoginStore = LoginStore;
    exports.LoginSuccessAction = LoginSuccessAction;
    exports.LogoutBeginAction = LogoutBeginAction;
    exports.LogoutFailAction = LogoutFailAction;
    exports.LogoutSuccessAction = LogoutSuccessAction;
    exports.NATIVE_STORAGE_SERVICE = NATIVE_STORAGE_SERVICE;
    exports.SetUserData = SetUserData;
    exports.SocialLoginBeginAction = SocialLoginBeginAction;
    exports.SocialLoginFailAction = SocialLoginFailAction;
    exports.SocialLoginSuccessAction = SocialLoginSuccessAction;
    exports.TOKEN_KEY = TOKEN_KEY;
    exports.UserModel = UserModel;
    exports.getError = getError;
    exports.getIsLoading = getIsLoading;
    exports.getLoginPageState = getLoginPageState;
    exports.getLoginState = getLoginState;
    exports.getUser = getUser;
    exports.initialState = initialState;
    exports.jwtOptionsFactory = jwtOptionsFactory;
    exports.loginReducer = loginReducer;
    exports.storeGetIsLoading = storeGetIsLoading;
    exports.storeGetUser = storeGetUser;
    exports.ɵ0 = ɵ0$1;
    exports.ɵ1 = ɵ1$1;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-login-core.umd.js.map
