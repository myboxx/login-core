import { __decorate, __param } from 'tslib';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

const LOGIN_REPOSITORY = new InjectionToken('loginRepository');

let LoginRepository = class LoginRepository {
    constructor(http, appSettings) {
        this.http = http;
        this.appSettings = appSettings;
        this.REFRESH_TOKEN_URL = '/api/v1/auth/refresh_token';
    }
    login(credentials) {
        let params = new HttpParams();
        for (const property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }
        const body = params.toString();
        return this.http.post(`${this.appSettings.baseUrl()}/api/v2/auth/login`, body);
    }
    socialLogin(credentials) {
        let params = new HttpParams();
        for (const property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }
        const body = params.toString();
        return this.http.post(`${this.appSettings.baseUrl()}/api/v2/auth/add_social`, body);
    }
    saveToken(deviceId, fcmRegistrationId) {
        const data = { device_id: deviceId, fcm_registration_id: fcmRegistrationId };
        let params = new HttpParams();
        for (const property in data) {
            if (data.hasOwnProperty(property)) {
                params = params.append(property, data[property]);
            }
        }
        const body = params.toString();
        return this.http.post(`${this.getBaseUrl()}/fcm/devices`, body);
    }
    logSessionStart(payload) {
        const params = new HttpParams()
            .append('action_type', payload.action_type);
        // .append('action_desciption', payload.action_description || 'NO_DESCRIPTION_SUPPLIED');
        const urlSearchParams = new URLSearchParams();
        urlSearchParams.append('action_type', payload.action_type);
        const body = params.toString();
        return this.http.post(`${this.appSettings.baseUrl()}/api/v1/profileUser/check_app_login`, body);
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1`;
    }
};
LoginRepository.ctorParameters = () => [
    { type: HttpClient },
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] }
];
LoginRepository = __decorate([
    Injectable(),
    __param(1, Inject(APP_CONFIG_SERVICE))
], LoginRepository);

const LOGIN_SERVICE = new InjectionToken('loginService');
const NATIVE_STORAGE_SERVICE = new InjectionToken('nativeStorageService');

class UserModel {
    constructor(data) {
        this.id = data.id;
        this.clientId = data.clientId;
        this.name = data.name;
        this.email = data.email;
        this.roleId = data.roleId;
        this.authedAt = data.authedAt;
        this.deviceId = data.deviceId;
    }
    static fromTokenData(data) {
        return new UserModel({
            id: data.id,
            clientId: data.client_id,
            name: data.name,
            email: data.email,
            roleId: data.role_id,
            authedAt: data.authed_at,
            deviceId: data.device_id,
        });
    }
    static empty() {
        return new UserModel({
            id: null,
            clientId: null,
            name: null,
            email: null,
            roleId: null,
            authedAt: null,
            deviceId: null,
        });
    }
}

const helper = new JwtHelperService();
const TOKEN_KEY = 'AUTH_TOKEN';
function jwtOptionsFactory(localStge) {
    return {
        tokenGetter: () => localStge.getItem(TOKEN_KEY)
    };
}
let LoginService = class LoginService {
    constructor(localStge, repository) {
        this.localStge = localStge;
        this.repository = repository;
    }
    login(credentials) {
        return this.repository.login(credentials).pipe(map(response => {
            if (response.status !== 'success' || !response.data.token) {
                throw new Error('No token was received');
            }
            this.localStge.setItem(TOKEN_KEY, response.data.token);
            const user = helper.decodeToken(response.data.token);
            const User = UserModel.fromTokenData(user);
            this.localStge.setItem('username', User.name);
            return User;
        }), catchError(e => {
            console.error('CATCH: AuthenticationService.login() -> repository.login()', e);
            throw (e);
        }));
    }
    socialLogin(credentials) {
        return this.repository.socialLogin(credentials).pipe(map(response => {
            if (response.status !== 'success' || !response.data.token) {
                throw new Error('No token was received');
            }
            this.localStge.setItem(TOKEN_KEY, response.data.token);
            const user = helper.decodeToken(response.data.token);
            const User = UserModel.fromTokenData(user);
            this.localStge.setItem('username', User.name);
            return User;
        }), catchError(e => {
            console.error('CATCH: AuthenticationService.socialLogin() -> repository.socialLogin()', e);
            throw (e);
        }));
    }
    logout() {
        return new Observable(subcriber => {
            this.localStge.clearStorage()
                .then(_ => {
                this.localStge.setItem('tutorial', 'OK');
                subcriber.next(true);
            })
                .catch(e => {
                console.error('AuthenticationService.logout()->this.localStge.clearStorage()', e);
                subcriber.error(e);
            });
        });
    }
    saveToken(deviceId, deviceToken) {
        return this.repository.saveToken(deviceId, deviceToken).pipe(map(response => {
            return response.data;
        }), catchError(e => {
            console.error('CATCH: AuthenticationService.saveToken() -> repository.saveToken()', e);
            throw (e);
        }));
    }
    logSessionStart() {
        return this.repository.logSessionStart({ action_type: 'login' }).pipe(map(response => {
            return response;
        }), catchError(error => {
            throw error;
        }));
    }
    getSession() {
        return this.localStge.getItem(TOKEN_KEY).then(token => {
            const user = helper.decodeToken(token);
            const User = UserModel.fromTokenData(user);
            return User;
        });
    }
    getUserName() {
        return this.localStge.getItem('username');
    }
};
LoginService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [NATIVE_STORAGE_SERVICE,] }] },
    { type: undefined, decorators: [{ type: Inject, args: [LOGIN_REPOSITORY,] }] }
];
LoginService.ɵprov = ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(ɵɵinject(NATIVE_STORAGE_SERVICE), ɵɵinject(LOGIN_REPOSITORY)); }, token: LoginService, providedIn: "root" });
LoginService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(NATIVE_STORAGE_SERVICE)),
    __param(1, Inject(LOGIN_REPOSITORY))
], LoginService);

var LoginActionTypes;
(function (LoginActionTypes) {
    LoginActionTypes["LoginBegin"] = "[Login] Login begin";
    LoginActionTypes["LoginSuccess"] = "[Login] Login success";
    LoginActionTypes["LoginFail"] = "[Login] Login failure";
    LoginActionTypes["SocialLoginBegin"] = "[Login] Social Login begin";
    LoginActionTypes["SocialLoginSuccess"] = "[Login] Social Login success";
    LoginActionTypes["SocialLoginFail"] = "[Login] Social Login failure";
    LoginActionTypes["LogoutBegin"] = "[Login] Logout begin";
    LoginActionTypes["LogoutSuccess"] = "[Login] Logout success";
    LoginActionTypes["LogoutFail"] = "[Login] Logout failure";
    LoginActionTypes["SetUserData"] = "[Login] Set User Data";
})(LoginActionTypes || (LoginActionTypes = {}));
// LOGIN...
const LoginBeginAction = createAction(LoginActionTypes.LoginBegin, props());
const SocialLoginBeginAction = createAction(LoginActionTypes.SocialLoginBegin, props());
const LoginSuccessAction = createAction(LoginActionTypes.LoginSuccess, props());
const SocialLoginSuccessAction = createAction(LoginActionTypes.SocialLoginSuccess, props());
const LoginFailAction = createAction(LoginActionTypes.LoginFail, props());
const SocialLoginFailAction = createAction(LoginActionTypes.SocialLoginFail, props());
// LOGOUT...
const LogoutBeginAction = createAction(LoginActionTypes.LogoutBegin);
const LogoutSuccessAction = createAction(LoginActionTypes.LogoutSuccess);
const LogoutFailAction = createAction(LoginActionTypes.LogoutFail, props());
const SetUserData = createAction(LoginActionTypes.SetUserData, props());

let LoginEffects = class LoginEffects {
    constructor(actions$, service) {
        this.actions$ = actions$;
        this.service = service;
        this.login$ = createEffect(() => this.actions$.pipe(ofType(LoginActionTypes.LoginBegin), switchMap((action) => {
            return this.service.login(action.credentials).pipe(map((user) => LoginSuccessAction({ user })), catchError(error => {
                console.error('Couldn\'t login', error);
                return of(LoginFailAction({ errors: error }));
            }));
        })));
        this.socialLogin$ = createEffect(() => this.actions$.pipe(ofType(LoginActionTypes.SocialLoginBegin), switchMap((action) => {
            return this.service.socialLogin(action.credentials).pipe(map((user) => SocialLoginSuccessAction({ user })), catchError(error => {
                console.error('Couldn\'t add Social login', error);
                return of(SocialLoginFailAction({ errors: error }));
            }));
        })));
        this.logout$ = createEffect(() => this.actions$.pipe(ofType(LoginActionTypes.LogoutBegin), switchMap(() => {
            return this.service.logout().pipe(map((success) => LogoutSuccessAction()), catchError(error => {
                console.error('Couldn\'t logout', error);
                return of(LogoutFailAction({ errors: error }));
            }));
        })));
    }
};
LoginEffects.ctorParameters = () => [
    { type: Actions },
    { type: undefined, decorators: [{ type: Inject, args: [LOGIN_SERVICE,] }] }
];
LoginEffects = __decorate([
    Injectable(),
    __param(1, Inject(LOGIN_SERVICE))
], LoginEffects);

const initialState = {
    isLoading: false,
    user: UserModel.empty(),
    errors: null
};
const ɵ0 = (state) => (Object.assign(Object.assign({}, state), { errors: null, isLoading: true })), ɵ1 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, user: action.user })), ɵ2 = (state) => (initialState), ɵ3 = (state, action) => (Object.assign(Object.assign({}, state), { user: action.user })), ɵ4 = (state, action) => ({
    isLoading: false,
    user: UserModel.empty(),
    errors: action.errors
});
const reducer = createReducer(initialState, 
// On Begin Actions
on(LoginBeginAction, SocialLoginBeginAction, LogoutBeginAction, ɵ0), 
// ON Success Actions
on(LoginSuccessAction, SocialLoginSuccessAction, ɵ1), on(LogoutSuccessAction, ɵ2), on(SetUserData, ɵ3), 
// ON Fail Actions
on(LoginFailAction, SocialLoginFailAction, LogoutFailAction, ɵ4));
function loginReducer(state, action) {
    return reducer(state, action);
}

const getLoginState = createFeatureSelector('login');
const ɵ0$1 = state => state;
const getLoginPageState = createSelector(getLoginState, ɵ0$1);
const storeGetIsLoading = (state) => state.isLoading;
const storeGetUser = (state) => state.user;
const getIsLoading = createSelector(getLoginPageState, storeGetIsLoading);
const getUser = createSelector(getLoginPageState, storeGetUser);
const ɵ1$1 = state => state.errors;
const getError = createSelector(getLoginPageState, ɵ1$1);

let LoginStore = class LoginStore {
    constructor(store) {
        this.store = store;
    }
    get User$() {
        return this.store.select(getUser);
    }
    get Loading$() {
        return this.store.select(getIsLoading);
    }
    get Error$() {
        return this.store.select(getError);
    }
    login(credentials) {
        this.store.dispatch(LoginBeginAction({ credentials }));
    }
    socialLogin(credentials) {
        this.store.dispatch(SocialLoginBeginAction({ credentials }));
    }
    logout() {
        this.store.dispatch(LogoutBeginAction());
    }
    setUserData(user) {
        this.store.dispatch(SetUserData({ user }));
    }
};
LoginStore.ctorParameters = () => [
    { type: Store }
];
LoginStore = __decorate([
    Injectable()
], LoginStore);

var LoginCoreModule_1;
let LoginCoreModule = LoginCoreModule_1 = class LoginCoreModule {
    static forRoot(config) {
        return {
            ngModule: LoginCoreModule_1,
            providers: [
                { provide: LOGIN_SERVICE, useClass: LoginService },
                { provide: LOGIN_REPOSITORY, useClass: LoginRepository },
                ...config.providers,
                LoginStore
            ]
        };
    }
};
LoginCoreModule = LoginCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('login', loginReducer),
            EffectsModule.forFeature([LoginEffects]),
        ],
        exports: []
    })
], LoginCoreModule);

/*
 * Public API Surface of login-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { LOGIN_REPOSITORY, LOGIN_SERVICE, LoginActionTypes, LoginBeginAction, LoginCoreModule, LoginEffects, LoginFailAction, LoginRepository, LoginService, LoginStore, LoginSuccessAction, LogoutBeginAction, LogoutFailAction, LogoutSuccessAction, NATIVE_STORAGE_SERVICE, SetUserData, SocialLoginBeginAction, SocialLoginFailAction, SocialLoginSuccessAction, TOKEN_KEY, UserModel, getError, getIsLoading, getLoginPageState, getLoginState, getUser, initialState, jwtOptionsFactory, loginReducer, storeGetIsLoading, storeGetUser, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1 };
//# sourceMappingURL=boxx-login-core.js.map
