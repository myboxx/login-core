import { __decorate, __read, __spread } from "tslib";
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LOGIN_REPOSITORY } from './repositories/ILogin.repository';
import { LoginRepository } from './repositories/login.repository';
import { LOGIN_SERVICE } from './services/ILogin.service';
import { LoginService } from './services/login.service';
import { LoginEffects } from './state/login.effects';
import { loginReducer } from './state/login.reducer';
import { LoginStore } from './state/login.store';
var LoginCoreModule = /** @class */ (function () {
    function LoginCoreModule() {
    }
    LoginCoreModule_1 = LoginCoreModule;
    LoginCoreModule.forRoot = function (config) {
        return {
            ngModule: LoginCoreModule_1,
            providers: __spread([
                { provide: LOGIN_SERVICE, useClass: LoginService },
                { provide: LOGIN_REPOSITORY, useClass: LoginRepository }
            ], config.providers, [
                LoginStore
            ])
        };
    };
    var LoginCoreModule_1;
    LoginCoreModule = LoginCoreModule_1 = __decorate([
        NgModule({
            declarations: [],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('login', loginReducer),
                EffectsModule.forFeature([LoginEffects]),
            ],
            exports: []
        })
    ], LoginCoreModule);
    return LoginCoreModule;
}());
export { LoginCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9sb2dpbi1jb3JlLyIsInNvdXJjZXMiOlsibGliL2xvZ2luLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQXVCLFFBQVEsRUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN4RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFlakQ7SUFBQTtJQWFDLENBQUM7d0JBYlcsZUFBZTtJQUNqQix1QkFBTyxHQUFkLFVBQWUsTUFBOEI7UUFFekMsT0FBTztZQUNILFFBQVEsRUFBRSxpQkFBZTtZQUN6QixTQUFTO2dCQUNMLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFO2dCQUNsRCxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFO2VBQ3JELE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixVQUFVO2NBQ2I7U0FDSixDQUFDO0lBQ04sQ0FBQzs7SUFaUSxlQUFlO1FBVDNCLFFBQVEsQ0FBQztZQUNOLFlBQVksRUFBRSxFQUFFO1lBQ2hCLE9BQU8sRUFBRTtnQkFDTCxnQkFBZ0I7Z0JBQ2hCLFdBQVcsQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQztnQkFDN0MsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQzNDO1lBQ0QsT0FBTyxFQUFFLEVBQUU7U0FDZCxDQUFDO09BQ1csZUFBZSxDQWExQjtJQUFELHNCQUFDO0NBQUEsQUFiRixJQWFFO1NBYlcsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEVmZmVjdHNNb2R1bGUgfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcbmltcG9ydCB7IFN0b3JlTW9kdWxlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgTE9HSU5fUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lMb2dpbi5yZXBvc2l0b3J5JztcbmltcG9ydCB7IExvZ2luUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL2xvZ2luLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgTE9HSU5fU0VSVklDRSB9IGZyb20gJy4vc2VydmljZXMvSUxvZ2luLnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9naW5TZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9sb2dpbi5zZXJ2aWNlJztcbmltcG9ydCB7IExvZ2luRWZmZWN0cyB9IGZyb20gJy4vc3RhdGUvbG9naW4uZWZmZWN0cyc7XG5pbXBvcnQgeyBsb2dpblJlZHVjZXIgfSBmcm9tICcuL3N0YXRlL2xvZ2luLnJlZHVjZXInO1xuaW1wb3J0IHsgTG9naW5TdG9yZSB9IGZyb20gJy4vc3RhdGUvbG9naW4uc3RvcmUnO1xuXG5pbnRlcmZhY2UgTW9kdWxlT3B0aW9uc0ludGVyZmFjZSB7XG4gICAgcHJvdmlkZXJzOiBQcm92aWRlcltdO1xufVxuXG5ATmdNb2R1bGUoe1xuICAgIGRlY2xhcmF0aW9uczogW10sXG4gICAgaW1wb3J0czogW1xuICAgICAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgICAgICBTdG9yZU1vZHVsZS5mb3JGZWF0dXJlKCdsb2dpbicsIGxvZ2luUmVkdWNlciksXG4gICAgICAgIEVmZmVjdHNNb2R1bGUuZm9yRmVhdHVyZShbTG9naW5FZmZlY3RzXSksXG4gICAgXSxcbiAgICBleHBvcnRzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkNvcmVNb2R1bGUge1xuICAgIHN0YXRpYyBmb3JSb290KGNvbmZpZzogTW9kdWxlT3B0aW9uc0ludGVyZmFjZSk6IE1vZHVsZVdpdGhQcm92aWRlcnM8TG9naW5Db3JlTW9kdWxlPiB7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG5nTW9kdWxlOiBMb2dpbkNvcmVNb2R1bGUsXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IExPR0lOX1NFUlZJQ0UsIHVzZUNsYXNzOiBMb2dpblNlcnZpY2UgfSxcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IExPR0lOX1JFUE9TSVRPUlksIHVzZUNsYXNzOiBMb2dpblJlcG9zaXRvcnkgfSxcbiAgICAgICAgICAgICAgICAuLi5jb25maWcucHJvdmlkZXJzLFxuICAgICAgICAgICAgICAgIExvZ2luU3RvcmVcbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICB9XG4gfVxuIl19