import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromSelector from './login.selectors';
import * as fromActions from './login.actions';
var LoginStore = /** @class */ (function () {
    function LoginStore(store) {
        this.store = store;
    }
    Object.defineProperty(LoginStore.prototype, "User$", {
        get: function () {
            return this.store.select(fromSelector.getUser);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginStore.prototype, "Loading$", {
        get: function () {
            return this.store.select(fromSelector.getIsLoading);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginStore.prototype, "Error$", {
        get: function () {
            return this.store.select(fromSelector.getError);
        },
        enumerable: true,
        configurable: true
    });
    LoginStore.prototype.login = function (credentials) {
        this.store.dispatch(fromActions.LoginBeginAction({ credentials: credentials }));
    };
    LoginStore.prototype.socialLogin = function (credentials) {
        this.store.dispatch(fromActions.SocialLoginBeginAction({ credentials: credentials }));
    };
    LoginStore.prototype.logout = function () {
        this.store.dispatch(fromActions.LogoutBeginAction());
    };
    LoginStore.prototype.setUserData = function (user) {
        this.store.dispatch(fromActions.SetUserData({ user: user }));
    };
    LoginStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    LoginStore = __decorate([
        Injectable()
    ], LoginStore);
    return LoginStore;
}());
export { LoginStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc3RvcmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9sb2dpbi1jb3JlLyIsInNvdXJjZXMiOlsibGliL3N0YXRlL2xvZ2luLnN0b3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDcEMsT0FBTyxLQUFLLFlBQVksTUFBTSxtQkFBbUIsQ0FBQztBQUVsRCxPQUFPLEtBQUssV0FBVyxNQUFNLGlCQUFpQixDQUFDO0FBTS9DO0lBQ0ksb0JBQW9CLEtBQW9DO1FBQXBDLFVBQUssR0FBTCxLQUFLLENBQStCO0lBQUksQ0FBQztJQUU3RCxzQkFBSSw2QkFBSzthQUFUO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxnQ0FBUTthQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw4QkFBTTthQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDcEQsQ0FBQzs7O09BQUE7SUFFRCwwQkFBSyxHQUFMLFVBQU0sV0FBMEM7UUFDNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsV0FBVyxhQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELGdDQUFXLEdBQVgsVUFBWSxXQUE2QjtRQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsRUFBRSxXQUFXLGFBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsMkJBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVELGdDQUFXLEdBQVgsVUFBWSxJQUFlO1FBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUMzRCxDQUFDOztnQkE1QjBCLEtBQUs7O0lBRHZCLFVBQVU7UUFEdEIsVUFBVSxFQUFFO09BQ0EsVUFBVSxDQThCdEI7SUFBRCxpQkFBQztDQUFBLEFBOUJELElBOEJDO1NBOUJZLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTdG9yZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCAqIGFzIGZyb21TZWxlY3RvciBmcm9tICcuL2xvZ2luLnNlbGVjdG9ycyc7XG5pbXBvcnQgKiBhcyBmcm9tUmVkdWNlciBmcm9tICcuL2xvZ2luLnJlZHVjZXInO1xuaW1wb3J0ICogYXMgZnJvbUFjdGlvbnMgZnJvbSAnLi9sb2dpbi5hY3Rpb25zJztcbmltcG9ydCB7IElMb2dpbkZvcm0sIElTb2NpYWxMb2dpbkZvcm0gfSBmcm9tICcuLi9yZXBvc2l0b3JpZXMvSUxvZ2luLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgVXNlck1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3VzZXIubW9kZWwnO1xuXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMb2dpblN0b3JlIHtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN0b3JlOiBTdG9yZTxmcm9tUmVkdWNlci5Mb2dpblN0YXRlPikgeyB9XG5cbiAgICBnZXQgVXNlciQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0VXNlcik7XG4gICAgfVxuXG4gICAgZ2V0IExvYWRpbmckKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldElzTG9hZGluZyk7XG4gICAgfVxuXG4gICAgZ2V0IEVycm9yJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRFcnJvcik7XG4gICAgfVxuXG4gICAgbG9naW4oY3JlZGVudGlhbHM6IElMb2dpbkZvcm0gfCBJU29jaWFsTG9naW5Gb3JtKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuTG9naW5CZWdpbkFjdGlvbih7IGNyZWRlbnRpYWxzIH0pKTtcbiAgICB9XG5cbiAgICBzb2NpYWxMb2dpbihjcmVkZW50aWFsczogSVNvY2lhbExvZ2luRm9ybSkge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLlNvY2lhbExvZ2luQmVnaW5BY3Rpb24oeyBjcmVkZW50aWFscyB9KSk7XG4gICAgfVxuXG4gICAgbG9nb3V0KCkge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkxvZ291dEJlZ2luQWN0aW9uKCkpO1xuICAgIH1cblxuICAgIHNldFVzZXJEYXRhKHVzZXI6IFVzZXJNb2RlbCkge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLlNldFVzZXJEYXRhKHsgdXNlciB9KSk7XG4gICAgfVxufVxuIl19