import { __decorate, __param, __assign, __spread } from 'tslib';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

var LOGIN_REPOSITORY = new InjectionToken('loginRepository');

var LoginRepository = /** @class */ (function () {
    function LoginRepository(http, appSettings) {
        this.http = http;
        this.appSettings = appSettings;
        this.REFRESH_TOKEN_URL = '/api/v1/auth/refresh_token';
    }
    LoginRepository.prototype.login = function (credentials) {
        var params = new HttpParams();
        for (var property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }
        var body = params.toString();
        return this.http.post(this.appSettings.baseUrl() + "/api/v2/auth/login", body);
    };
    LoginRepository.prototype.socialLogin = function (credentials) {
        var params = new HttpParams();
        for (var property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }
        var body = params.toString();
        return this.http.post(this.appSettings.baseUrl() + "/api/v2/auth/add_social", body);
    };
    LoginRepository.prototype.saveToken = function (deviceId, fcmRegistrationId) {
        var data = { device_id: deviceId, fcm_registration_id: fcmRegistrationId };
        var params = new HttpParams();
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                params = params.append(property, data[property]);
            }
        }
        var body = params.toString();
        return this.http.post(this.getBaseUrl() + "/fcm/devices", body);
    };
    LoginRepository.prototype.logSessionStart = function (payload) {
        var params = new HttpParams()
            .append('action_type', payload.action_type);
        // .append('action_desciption', payload.action_description || 'NO_DESCRIPTION_SUPPLIED');
        var urlSearchParams = new URLSearchParams();
        urlSearchParams.append('action_type', payload.action_type);
        var body = params.toString();
        return this.http.post(this.appSettings.baseUrl() + "/api/v1/profileUser/check_app_login", body);
    };
    LoginRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1";
    };
    LoginRepository.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] }
    ]; };
    LoginRepository = __decorate([
        Injectable(),
        __param(1, Inject(APP_CONFIG_SERVICE))
    ], LoginRepository);
    return LoginRepository;
}());

var LOGIN_SERVICE = new InjectionToken('loginService');
var NATIVE_STORAGE_SERVICE = new InjectionToken('nativeStorageService');

var UserModel = /** @class */ (function () {
    function UserModel(data) {
        this.id = data.id;
        this.clientId = data.clientId;
        this.name = data.name;
        this.email = data.email;
        this.roleId = data.roleId;
        this.authedAt = data.authedAt;
        this.deviceId = data.deviceId;
    }
    UserModel.fromTokenData = function (data) {
        return new UserModel({
            id: data.id,
            clientId: data.client_id,
            name: data.name,
            email: data.email,
            roleId: data.role_id,
            authedAt: data.authed_at,
            deviceId: data.device_id,
        });
    };
    UserModel.empty = function () {
        return new UserModel({
            id: null,
            clientId: null,
            name: null,
            email: null,
            roleId: null,
            authedAt: null,
            deviceId: null,
        });
    };
    return UserModel;
}());

var helper = new JwtHelperService();
var TOKEN_KEY = 'AUTH_TOKEN';
function jwtOptionsFactory(localStge) {
    return {
        tokenGetter: function () { return localStge.getItem(TOKEN_KEY); }
    };
}
var LoginService = /** @class */ (function () {
    function LoginService(localStge, repository) {
        this.localStge = localStge;
        this.repository = repository;
    }
    LoginService.prototype.login = function (credentials) {
        var _this = this;
        return this.repository.login(credentials).pipe(map(function (response) {
            if (response.status !== 'success' || !response.data.token) {
                throw new Error('No token was received');
            }
            _this.localStge.setItem(TOKEN_KEY, response.data.token);
            var user = helper.decodeToken(response.data.token);
            var User = UserModel.fromTokenData(user);
            _this.localStge.setItem('username', User.name);
            return User;
        }), catchError(function (e) {
            console.error('CATCH: AuthenticationService.login() -> repository.login()', e);
            throw (e);
        }));
    };
    LoginService.prototype.socialLogin = function (credentials) {
        var _this = this;
        return this.repository.socialLogin(credentials).pipe(map(function (response) {
            if (response.status !== 'success' || !response.data.token) {
                throw new Error('No token was received');
            }
            _this.localStge.setItem(TOKEN_KEY, response.data.token);
            var user = helper.decodeToken(response.data.token);
            var User = UserModel.fromTokenData(user);
            _this.localStge.setItem('username', User.name);
            return User;
        }), catchError(function (e) {
            console.error('CATCH: AuthenticationService.socialLogin() -> repository.socialLogin()', e);
            throw (e);
        }));
    };
    LoginService.prototype.logout = function () {
        var _this = this;
        return new Observable(function (subcriber) {
            _this.localStge.clearStorage()
                .then(function (_) {
                _this.localStge.setItem('tutorial', 'OK');
                subcriber.next(true);
            })
                .catch(function (e) {
                console.error('AuthenticationService.logout()->this.localStge.clearStorage()', e);
                subcriber.error(e);
            });
        });
    };
    LoginService.prototype.saveToken = function (deviceId, deviceToken) {
        return this.repository.saveToken(deviceId, deviceToken).pipe(map(function (response) {
            return response.data;
        }), catchError(function (e) {
            console.error('CATCH: AuthenticationService.saveToken() -> repository.saveToken()', e);
            throw (e);
        }));
    };
    LoginService.prototype.logSessionStart = function () {
        return this.repository.logSessionStart({ action_type: 'login' }).pipe(map(function (response) {
            return response;
        }), catchError(function (error) {
            throw error;
        }));
    };
    LoginService.prototype.getSession = function () {
        return this.localStge.getItem(TOKEN_KEY).then(function (token) {
            var user = helper.decodeToken(token);
            var User = UserModel.fromTokenData(user);
            return User;
        });
    };
    LoginService.prototype.getUserName = function () {
        return this.localStge.getItem('username');
    };
    LoginService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NATIVE_STORAGE_SERVICE,] }] },
        { type: undefined, decorators: [{ type: Inject, args: [LOGIN_REPOSITORY,] }] }
    ]; };
    LoginService.ɵprov = ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(ɵɵinject(NATIVE_STORAGE_SERVICE), ɵɵinject(LOGIN_REPOSITORY)); }, token: LoginService, providedIn: "root" });
    LoginService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(NATIVE_STORAGE_SERVICE)),
        __param(1, Inject(LOGIN_REPOSITORY))
    ], LoginService);
    return LoginService;
}());

var LoginActionTypes;
(function (LoginActionTypes) {
    LoginActionTypes["LoginBegin"] = "[Login] Login begin";
    LoginActionTypes["LoginSuccess"] = "[Login] Login success";
    LoginActionTypes["LoginFail"] = "[Login] Login failure";
    LoginActionTypes["SocialLoginBegin"] = "[Login] Social Login begin";
    LoginActionTypes["SocialLoginSuccess"] = "[Login] Social Login success";
    LoginActionTypes["SocialLoginFail"] = "[Login] Social Login failure";
    LoginActionTypes["LogoutBegin"] = "[Login] Logout begin";
    LoginActionTypes["LogoutSuccess"] = "[Login] Logout success";
    LoginActionTypes["LogoutFail"] = "[Login] Logout failure";
    LoginActionTypes["SetUserData"] = "[Login] Set User Data";
})(LoginActionTypes || (LoginActionTypes = {}));
// LOGIN...
var LoginBeginAction = createAction(LoginActionTypes.LoginBegin, props());
var SocialLoginBeginAction = createAction(LoginActionTypes.SocialLoginBegin, props());
var LoginSuccessAction = createAction(LoginActionTypes.LoginSuccess, props());
var SocialLoginSuccessAction = createAction(LoginActionTypes.SocialLoginSuccess, props());
var LoginFailAction = createAction(LoginActionTypes.LoginFail, props());
var SocialLoginFailAction = createAction(LoginActionTypes.SocialLoginFail, props());
// LOGOUT...
var LogoutBeginAction = createAction(LoginActionTypes.LogoutBegin);
var LogoutSuccessAction = createAction(LoginActionTypes.LogoutSuccess);
var LogoutFailAction = createAction(LoginActionTypes.LogoutFail, props());
var SetUserData = createAction(LoginActionTypes.SetUserData, props());

var LoginEffects = /** @class */ (function () {
    function LoginEffects(actions$, service) {
        var _this = this;
        this.actions$ = actions$;
        this.service = service;
        this.login$ = createEffect(function () { return _this.actions$.pipe(ofType(LoginActionTypes.LoginBegin), switchMap(function (action) {
            return _this.service.login(action.credentials).pipe(map(function (user) { return LoginSuccessAction({ user: user }); }), catchError(function (error) {
                console.error('Couldn\'t login', error);
                return of(LoginFailAction({ errors: error }));
            }));
        })); });
        this.socialLogin$ = createEffect(function () { return _this.actions$.pipe(ofType(LoginActionTypes.SocialLoginBegin), switchMap(function (action) {
            return _this.service.socialLogin(action.credentials).pipe(map(function (user) { return SocialLoginSuccessAction({ user: user }); }), catchError(function (error) {
                console.error('Couldn\'t add Social login', error);
                return of(SocialLoginFailAction({ errors: error }));
            }));
        })); });
        this.logout$ = createEffect(function () { return _this.actions$.pipe(ofType(LoginActionTypes.LogoutBegin), switchMap(function () {
            return _this.service.logout().pipe(map(function (success) { return LogoutSuccessAction(); }), catchError(function (error) {
                console.error('Couldn\'t logout', error);
                return of(LogoutFailAction({ errors: error }));
            }));
        })); });
    }
    LoginEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: undefined, decorators: [{ type: Inject, args: [LOGIN_SERVICE,] }] }
    ]; };
    LoginEffects = __decorate([
        Injectable(),
        __param(1, Inject(LOGIN_SERVICE))
    ], LoginEffects);
    return LoginEffects;
}());

var initialState = {
    isLoading: false,
    user: UserModel.empty(),
    errors: null
};
var ɵ0 = function (state) { return (__assign(__assign({}, state), { errors: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, user: action.user })); }, ɵ2 = function (state) { return (initialState); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { user: action.user })); }, ɵ4 = function (state, action) { return ({
    isLoading: false,
    user: UserModel.empty(),
    errors: action.errors
}); };
var reducer = createReducer(initialState, 
// On Begin Actions
on(LoginBeginAction, SocialLoginBeginAction, LogoutBeginAction, ɵ0), 
// ON Success Actions
on(LoginSuccessAction, SocialLoginSuccessAction, ɵ1), on(LogoutSuccessAction, ɵ2), on(SetUserData, ɵ3), 
// ON Fail Actions
on(LoginFailAction, SocialLoginFailAction, LogoutFailAction, ɵ4));
function loginReducer(state, action) {
    return reducer(state, action);
}

var getLoginState = createFeatureSelector('login');
var ɵ0$1 = function (state) { return state; };
var getLoginPageState = createSelector(getLoginState, ɵ0$1);
var storeGetIsLoading = function (state) { return state.isLoading; };
var storeGetUser = function (state) { return state.user; };
var getIsLoading = createSelector(getLoginPageState, storeGetIsLoading);
var getUser = createSelector(getLoginPageState, storeGetUser);
var ɵ1$1 = function (state) { return state.errors; };
var getError = createSelector(getLoginPageState, ɵ1$1);

var LoginStore = /** @class */ (function () {
    function LoginStore(store) {
        this.store = store;
    }
    Object.defineProperty(LoginStore.prototype, "User$", {
        get: function () {
            return this.store.select(getUser);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginStore.prototype, "Loading$", {
        get: function () {
            return this.store.select(getIsLoading);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginStore.prototype, "Error$", {
        get: function () {
            return this.store.select(getError);
        },
        enumerable: true,
        configurable: true
    });
    LoginStore.prototype.login = function (credentials) {
        this.store.dispatch(LoginBeginAction({ credentials: credentials }));
    };
    LoginStore.prototype.socialLogin = function (credentials) {
        this.store.dispatch(SocialLoginBeginAction({ credentials: credentials }));
    };
    LoginStore.prototype.logout = function () {
        this.store.dispatch(LogoutBeginAction());
    };
    LoginStore.prototype.setUserData = function (user) {
        this.store.dispatch(SetUserData({ user: user }));
    };
    LoginStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    LoginStore = __decorate([
        Injectable()
    ], LoginStore);
    return LoginStore;
}());

var LoginCoreModule = /** @class */ (function () {
    function LoginCoreModule() {
    }
    LoginCoreModule_1 = LoginCoreModule;
    LoginCoreModule.forRoot = function (config) {
        return {
            ngModule: LoginCoreModule_1,
            providers: __spread([
                { provide: LOGIN_SERVICE, useClass: LoginService },
                { provide: LOGIN_REPOSITORY, useClass: LoginRepository }
            ], config.providers, [
                LoginStore
            ])
        };
    };
    var LoginCoreModule_1;
    LoginCoreModule = LoginCoreModule_1 = __decorate([
        NgModule({
            declarations: [],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('login', loginReducer),
                EffectsModule.forFeature([LoginEffects]),
            ],
            exports: []
        })
    ], LoginCoreModule);
    return LoginCoreModule;
}());

/*
 * Public API Surface of login-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { LOGIN_REPOSITORY, LOGIN_SERVICE, LoginActionTypes, LoginBeginAction, LoginCoreModule, LoginEffects, LoginFailAction, LoginRepository, LoginService, LoginStore, LoginSuccessAction, LogoutBeginAction, LogoutFailAction, LogoutSuccessAction, NATIVE_STORAGE_SERVICE, SetUserData, SocialLoginBeginAction, SocialLoginFailAction, SocialLoginSuccessAction, TOKEN_KEY, UserModel, getError, getIsLoading, getLoginPageState, getLoginState, getUser, initialState, jwtOptionsFactory, loginReducer, storeGetIsLoading, storeGetUser, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1 };
//# sourceMappingURL=boxx-login-core.js.map
