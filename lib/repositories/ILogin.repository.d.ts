import { InjectionToken } from '@angular/core';
import { IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
export interface ILoginRepository {
    login(form: ILoginForm | ISocialLoginForm): Observable<IHttpBasicResponse<ILoginDataResponse>>;
    socialLogin(form: ISocialLoginForm): Observable<IHttpBasicResponse<ISocialLoginDataResponse>>;
    saveToken(deviceId: string, deviceToken: string): Observable<IHttpBasicResponse<any>>;
    logSessionStart(payload: ILogSessionPayload): Observable<IHttpBasicResponse<any>>;
}
export declare const LOGIN_REPOSITORY: InjectionToken<ILoginRepository>;
export interface ILoginForm {
    user: string;
    password: string;
    device_id: string;
    device_model: string;
}
export interface ILoginDataResponse {
    token: string;
}
export interface ISocialLoginForm {
    email: string;
    uid: string;
    type: 'facebook' | 'google' | 'unknown';
}
export interface ISocialLoginDataResponse {
    token: string;
}
export interface ISaveTokenDataResponse {
    user_id: string;
    device_id: string;
    fcm_registration_id: string;
    created_at: string;
}
export interface ILogSessionPayload {
    action_type: string;
    action_description?: string;
}
