import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ILoginDataResponse, ILoginForm, ILoginRepository, ILogSessionPayload, ISaveTokenDataResponse, ISocialLoginDataResponse, ISocialLoginForm } from './ILogin.repository';
import { IHttpBasicResponse, AbstractAppConfigService } from '@boxx/core';
export declare class LoginRepository implements ILoginRepository {
    private http;
    private appSettings;
    readonly REFRESH_TOKEN_URL: string;
    constructor(http: HttpClient, appSettings: AbstractAppConfigService);
    login(credentials: ILoginForm | ISocialLoginForm): Observable<IHttpBasicResponse<ILoginDataResponse>>;
    socialLogin(credentials: ISocialLoginForm): Observable<IHttpBasicResponse<ISocialLoginDataResponse>>;
    saveToken(deviceId: string, fcmRegistrationId: string): Observable<IHttpBasicResponse<ISaveTokenDataResponse>>;
    logSessionStart(payload: ILogSessionPayload): Observable<IHttpBasicResponse<any>>;
    private getBaseUrl;
}
