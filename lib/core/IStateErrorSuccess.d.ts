import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';
export interface ILoginStateError extends IStateErrorBase {
    after: 'LOGIN' | 'LOGOUT' | 'UNKNOWN';
}
export interface ILoginStateSuccess extends IStateSuccessBase {
    after: 'LOGIN' | 'LOGOUT' | 'UNKNOWN';
}
