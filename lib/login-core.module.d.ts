import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class LoginCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<LoginCoreModule>;
}
export {};
