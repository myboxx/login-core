export declare class UserModel implements IUserProps {
    id: number;
    clientId: number;
    name: string;
    email: string;
    roleId: number;
    authedAt: string;
    deviceId: string;
    constructor(data: IUserProps);
    static fromTokenData(data: any): UserModel;
    static empty(): UserModel;
}
export interface IUserProps {
    id: number;
    clientId: number;
    name: string;
    email: string;
    roleId: number;
    authedAt: string;
    deviceId: string;
}
