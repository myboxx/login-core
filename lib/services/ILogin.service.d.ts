import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
import { ILoginForm, ILogSessionPayload, ISocialLoginForm } from '../repositories/ILogin.repository';
export interface ILoginService {
    login(form: ILoginForm | ISocialLoginForm): Observable<UserModel>;
    socialLogin(form: ISocialLoginForm): Observable<UserModel>;
    logout(): Observable<boolean>;
    saveToken(deviceId: string, deviceToken: string): Observable<any>;
    logSessionStart(payload: ILogSessionPayload): Observable<any>;
    getSession(): Promise<UserModel>;
    getUserName(): Promise<string>;
}
export declare const LOGIN_SERVICE: InjectionToken<ILoginService>;
export interface INativeStorageService {
    getItem(key: string): Promise<string>;
    setItem(key: string, value: any): Promise<any>;
    removeItem(key: string): Promise<any>;
    keys(): Promise<Array<string>>;
    clearStorage(): Promise<any>;
}
export declare const NATIVE_STORAGE_SERVICE: InjectionToken<INativeStorageService>;
