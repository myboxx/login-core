import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
import { ILoginForm, ILoginRepository, ISaveTokenDataResponse, ISocialLoginForm } from '../repositories/ILogin.repository';
import { ILoginService, INativeStorageService } from './ILogin.service';
export declare const TOKEN_KEY = "AUTH_TOKEN";
export declare function jwtOptionsFactory(localStge: INativeStorageService): {
    tokenGetter: () => Promise<string>;
};
export declare class LoginService implements ILoginService {
    private localStge;
    private repository;
    constructor(localStge: INativeStorageService, repository: ILoginRepository);
    login(credentials: ILoginForm | ISocialLoginForm): Observable<UserModel>;
    socialLogin(credentials: ISocialLoginForm): Observable<UserModel>;
    logout(): Observable<boolean>;
    saveToken(deviceId: string, deviceToken: string): Observable<ISaveTokenDataResponse>;
    logSessionStart(): Observable<import("@boxx/core").IHttpBasicResponse<any>>;
    getSession(): Promise<UserModel>;
    getUserName(): Promise<string>;
}
