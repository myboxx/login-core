import { Store } from '@ngrx/store';
import * as fromReducer from './login.reducer';
import { ILoginForm, ISocialLoginForm } from '../repositories/ILogin.repository';
import { UserModel } from '../models/user.model';
export declare class LoginStore {
    private store;
    constructor(store: Store<fromReducer.LoginState>);
    get User$(): import("rxjs").Observable<UserModel>;
    get Loading$(): import("rxjs").Observable<boolean>;
    get Error$(): import("rxjs").Observable<any>;
    login(credentials: ILoginForm | ISocialLoginForm): void;
    socialLogin(credentials: ISocialLoginForm): void;
    logout(): void;
    setUserData(user: UserModel): void;
}
