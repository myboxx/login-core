import { Actions } from '@ngrx/effects';
import * as fromActions from './login.actions';
import { ILoginService } from '../services/ILogin.service';
import { UserModel } from '../models/user.model';
export declare class LoginEffects {
    private actions$;
    private service;
    login$: import("rxjs").Observable<({
        user: UserModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.LoginActionTypes.LoginSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.LoginActionTypes.LoginFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    socialLogin$: import("rxjs").Observable<({
        user: UserModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.LoginActionTypes.SocialLoginSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.LoginActionTypes.SocialLoginFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    logout$: import("rxjs").Observable<import("@ngrx/store/src/models").TypedAction<fromActions.LoginActionTypes.LogoutSuccess> | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.LoginActionTypes.LogoutFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, service: ILoginService);
}
