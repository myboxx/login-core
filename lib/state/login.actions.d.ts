import { UserModel } from '../models/user.model';
import { ILoginForm, ISocialLoginForm } from '../repositories/ILogin.repository';
export declare enum LoginActionTypes {
    LoginBegin = "[Login] Login begin",
    LoginSuccess = "[Login] Login success",
    LoginFail = "[Login] Login failure",
    SocialLoginBegin = "[Login] Social Login begin",
    SocialLoginSuccess = "[Login] Social Login success",
    SocialLoginFail = "[Login] Social Login failure",
    LogoutBegin = "[Login] Logout begin",
    LogoutSuccess = "[Login] Logout success",
    LogoutFail = "[Login] Logout failure",
    SetUserData = "[Login] Set User Data"
}
export declare const LoginBeginAction: import("@ngrx/store").ActionCreator<LoginActionTypes.LoginBegin, (props: {
    credentials: ILoginForm | ISocialLoginForm;
}) => {
    credentials: ILoginForm | ISocialLoginForm;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.LoginBegin>>;
export declare const SocialLoginBeginAction: import("@ngrx/store").ActionCreator<LoginActionTypes.SocialLoginBegin, (props: {
    credentials: ISocialLoginForm;
}) => {
    credentials: ISocialLoginForm;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.SocialLoginBegin>>;
export declare const LoginSuccessAction: import("@ngrx/store").ActionCreator<LoginActionTypes.LoginSuccess, (props: {
    user: UserModel;
}) => {
    user: UserModel;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.LoginSuccess>>;
export declare const SocialLoginSuccessAction: import("@ngrx/store").ActionCreator<LoginActionTypes.SocialLoginSuccess, (props: {
    user: UserModel;
}) => {
    user: UserModel;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.SocialLoginSuccess>>;
export declare const LoginFailAction: import("@ngrx/store").ActionCreator<LoginActionTypes.LoginFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.LoginFail>>;
export declare const SocialLoginFailAction: import("@ngrx/store").ActionCreator<LoginActionTypes.SocialLoginFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.SocialLoginFail>>;
export declare const LogoutBeginAction: import("@ngrx/store").ActionCreator<LoginActionTypes.LogoutBegin, () => import("@ngrx/store/src/models").TypedAction<LoginActionTypes.LogoutBegin>>;
export declare const LogoutSuccessAction: import("@ngrx/store").ActionCreator<LoginActionTypes.LogoutSuccess, () => import("@ngrx/store/src/models").TypedAction<LoginActionTypes.LogoutSuccess>>;
export declare const LogoutFailAction: import("@ngrx/store").ActionCreator<LoginActionTypes.LogoutFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.LogoutFail>>;
export declare const SetUserData: import("@ngrx/store").ActionCreator<LoginActionTypes.SetUserData, (props: {
    user: UserModel;
}) => {
    user: UserModel;
} & import("@ngrx/store/src/models").TypedAction<LoginActionTypes.SetUserData>>;
