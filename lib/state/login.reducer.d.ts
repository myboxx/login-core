import { Action } from '@ngrx/store';
import { UserModel } from '../models/user.model';
export interface LoginState {
    isLoading: boolean;
    user: UserModel;
    errors: any;
}
export declare const initialState: LoginState;
export declare function loginReducer(state: LoginState | undefined, action: Action): LoginState;
