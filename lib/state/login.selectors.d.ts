import * as fromReducer from './login.reducer';
export declare const getLoginState: import("@ngrx/store").MemoizedSelector<object, fromReducer.LoginState, import("@ngrx/store").DefaultProjectorFn<fromReducer.LoginState>>;
export declare const getLoginPageState: import("@ngrx/store").MemoizedSelector<object, fromReducer.LoginState, import("@ngrx/store").DefaultProjectorFn<fromReducer.LoginState>>;
export declare const storeGetIsLoading: (state: fromReducer.LoginState) => boolean;
export declare const storeGetUser: (state: fromReducer.LoginState) => import("../models/user.model").UserModel;
export declare const getIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getUser: import("@ngrx/store").MemoizedSelector<object, import("../models/user.model").UserModel, import("@ngrx/store").DefaultProjectorFn<import("../models/user.model").UserModel>>;
export declare const getError: import("@ngrx/store").MemoizedSelector<object, any, import("@ngrx/store").DefaultProjectorFn<any>>;
